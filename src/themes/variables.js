export const colors = {
  themeColor: '#1B2437',
  secondaryColor: '#EDA3B5',
  black: '#1B2437',
  white: '#FFF',
  blue: '#264653',
  light: '#EBF0FF',
  naturalDark: '#223263',
  neutralGrey: '#9098B1',
  neutralLight: '#EBF0FF',
  red: '#E76F51',
  green: '#2A9D8F',
  orange: '#F4A261',
  yellow: '#E9C46A',
  darkBlue: '#264653',
  pink: '#EDA3B5',
  gray1: '#EDA3B5',
  gray2: '#EFEFF4',
  gray3: '#E5E5EA',
  gray4: '#D1D1D6',
  gray5: '#C7C7CC',
  gray6: '#8E8E93',
  gray7: '#48484A'
};

export const sizes = {
  tiny: 10,
  small: 15,
  medium: 20,
  large: 25
};

export const fontSizes = {
  tiny: 10,
  small: 12,
  medium: 14,
  large: 16,
  xlarge: 18,
  xxlarge: 20
};

export const fontFamilies = {
  primaryRegular: 'PlayfairDisplay-Regular',
  secondaryRegular: 'Mulish-Regular'
};

export const NavigationTheme = {
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: 'rgb(255, 255, 255)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)'
  }
};
