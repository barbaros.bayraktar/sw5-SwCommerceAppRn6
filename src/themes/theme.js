export const theme = {
  colors: {
    violet100: '#e1e1e1',
    primary: '#00A8EC',
    secondary: '#53D1B6',
    red: '#FB7181',
    white: '#ffffff',
    grey: '#9098B1',
    light: '#EBF0FF',
    dark: '#223263'
  },
  components: {
    Button: {
      variants: {
        red: {
          backgroundColor: 'green'
        }
      }
    }
  }
};

export const fontFamilies = {
  primaryRegular: 'PlayfairDisplay-Regular',
  secondary: 'PlayfairDisplay-Regular',
  secondaryRegular: 'Mulish-Regular'
};
