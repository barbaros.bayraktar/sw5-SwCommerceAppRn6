export default {
  HOME: 'Home',
  LOGIN: 'Login',
  PROFILE: 'My Profile',
  PROFILEEDIT: 'Profile Edit',
  ORDERS: 'My Orders',
  SETTINGS: 'Settings'
};
