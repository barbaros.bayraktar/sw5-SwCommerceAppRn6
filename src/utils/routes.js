export default {
  homeScreen: 'HomeScreen',
  userEditScreen: 'UserEditScreen',
  productDetailScreen: 'ProductDetailScreen',
  categoriesScreen: 'CategoriesScreen',
  cartScreen: 'CartScreen',
  userScreen: 'UserScreen',
  loginScreen: 'UserLoginScreen',
  userOrderScreen: 'UserOrderScreen',
  settingsScreen: 'SettingsScreen',
  tabComponent: 'TabComponent'
};
