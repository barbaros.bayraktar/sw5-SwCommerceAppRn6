import styled from 'styled-components/native';
import FastImage from 'react-native-fast-image';

export const MediaImage = styled(FastImage)`
  height: 100%;
  width: 100%;
`;
