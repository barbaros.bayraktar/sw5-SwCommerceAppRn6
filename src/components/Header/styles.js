import styled from 'styled-components/native';

const IconContainer = styled.View`
  height: 30px;
  margin-left: 20px;
  width: 30px;
`;

const RightHeaderContainer = styled.View`
  height: 30px;
  margin-right: 20px;
  width: 30px;
`;

export const Styled = {
  IconContainer,
  RightHeaderContainer,
};
