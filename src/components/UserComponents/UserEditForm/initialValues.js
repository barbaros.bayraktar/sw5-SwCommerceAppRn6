export const initialValues = {
  email: (customerData) => customerData.email,
  firstname: (customerData) => customerData.firstname,
  lastname: (customerData) => customerData.lastname,
  newsletter: (customerData) => customerData.newsletter,
  salutation: (customerData) => customerData.salutation
};
