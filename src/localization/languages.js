export const languages = [
  {
    name: 'Deutsch',
    locale: 'de',
    id: 3,
    translateId: 0,
  },
  {
    name: 'English',
    locale: 'en',
    id: 4,
    translateId: 3,
  },
  {
    name: 'French',
    locale: 'fr',
    id: 62,
    translateId: 4,
  },
];
