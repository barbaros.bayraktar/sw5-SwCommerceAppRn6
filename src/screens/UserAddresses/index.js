import React from 'react';

import UserAddressList from 'components/UserComponents/UserAddressList';

export default function UserAddresses() {
  return <UserAddressList />;
}
