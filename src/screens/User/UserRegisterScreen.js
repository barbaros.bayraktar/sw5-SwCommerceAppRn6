import React from 'react';
import { SafeAreaView } from 'react-native';
import UserRegisterForm from '../../components/UserComponents/UserRegisterForm';

export default function UserRegisterScreen() {
  return (
    <SafeAreaView>
      <UserRegisterForm />
    </SafeAreaView>
  );
}
