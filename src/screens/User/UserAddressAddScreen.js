import React from 'react';

import { Container } from 'themes/components';
import UserAddressAddEditForm from 'components/UserComponents/UserAddressAddEditForm';

export default function UserAddressAddScreen() {
  return (
    <Container>
      <UserAddressAddEditForm />
    </Container>
  );
}
