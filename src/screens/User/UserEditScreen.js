import React from 'react';

import UserEditForm from 'components/UserComponents/UserEditForm';

const UserEditScreen = () => <UserEditForm />;

export default UserEditScreen;
