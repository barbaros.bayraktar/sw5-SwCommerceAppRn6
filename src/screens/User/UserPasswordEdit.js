import React from 'react';

import UserPasswordEditForm from '../../components/UserComponents/UserPasswordEditForm';

export default function UserPasswordEdit() {
  return <UserPasswordEditForm />;
}
